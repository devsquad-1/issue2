package com.optimum.bigdecimal;

import java.math.BigDecimal;

public class ComparingApplication {

	public static void main(String[] args) {

		UsingBigDecimal refUsingBigDecimal = new UsingBigDecimal();
		BigDecimal firstNumBD = new BigDecimal(10.53698);
		BigDecimal secondNumBD = new BigDecimal(2.98856);
		
		System.out.println("BigDecimal Addition: " + refUsingBigDecimal.add(firstNumBD, secondNumBD));
		System.out.println("BigDecimal Subtraction: " + refUsingBigDecimal.subtract(firstNumBD, secondNumBD));
		System.out.println("BigDecimal Multiplication: " + refUsingBigDecimal.multiply(firstNumBD, secondNumBD));
		System.out.println("BigDecimal Division: " + refUsingBigDecimal.divide(firstNumBD, secondNumBD));

		UsingDouble refUsingDouble = new UsingDouble();
		double firstNumDouble = 10.53698;
		double secondNumDouble = 2.98856;
		
		System.out.println("\ndouble Addition: " + refUsingDouble.add(firstNumDouble, secondNumDouble));
		System.out.println("double Subtraction: " + refUsingDouble.subtract(firstNumDouble, secondNumDouble));
		System.out.println("double Multiplication: " + refUsingDouble.multiply(firstNumDouble, secondNumDouble));
		System.out.println("double Division: " + refUsingDouble.divide(firstNumDouble, secondNumDouble));
	}

}
