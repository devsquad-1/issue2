package com.optimum.bigdecimal;

public class UsingDouble {

	double firstNum;
	double secondNum;
	
	public double add(double firstNum, double secondNum) {
		return firstNum + secondNum;
	}
	
	public double subtract(double firstNum, double secondNum) {
		return firstNum - secondNum;
	}
	
	public double multiply(double firstNum, double secondNum) {
		return firstNum * secondNum;
	}
	public double divide(double firstNum, double secondNum) {
		return firstNum / secondNum;
	}
	
}
