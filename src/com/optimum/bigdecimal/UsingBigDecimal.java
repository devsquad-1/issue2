package com.optimum.bigdecimal;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class UsingBigDecimal extends ComparingApplication {

	BigDecimal firstNum;
	BigDecimal secondNum;
	
	public BigDecimal add(BigDecimal firstNum, BigDecimal secondNum) {
		return firstNum.add(secondNum);
	}
	
	public BigDecimal subtract(BigDecimal firstNum, BigDecimal secondNum) {
		return firstNum.subtract(secondNum);
	}
	
	public BigDecimal multiply(BigDecimal firstNum, BigDecimal secondNum) {
		return firstNum.multiply(secondNum);
	}
	public BigDecimal divide(BigDecimal firstNum, BigDecimal secondNum) {
		return firstNum.divide(secondNum, 10, RoundingMode.CEILING);
	}

}
